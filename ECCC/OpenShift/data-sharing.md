Using a [journey diagram](https://mermaid.js.org/syntax/userJourney.html) to "... describe the current (as-is) user workflow, and reveals areas of improvement for the to-be workflow".

It would be nice to document those journeys as a funtion of user (e.g. "MSC specialist", "External User"), giving a pseudo use-case diagram.

```mermaid
journey
    title MSC data
    section Prepare
      Download: 1: MSC specialist, External user
      Configure env: 2: MSC specialist
    section Work
      Analyze: 5: MSC specialist,External user
      Visualize: 3: MSC specialist,External user
      Other: 4: MSC specialist,External user
      
```
