```mermaid
gantt
    title PMTiles Gantt Diagram

    dateFormat  YYYY-MM-DD
    axisFormat  %m/%d

    section Tasks
    Upload PMTile file to OSOS :done, a1, 2023-08-01, 2d
    Test with a Leaflet client :done, a2, 2023-08-03, 3d
    K8S CronJob to keep file updated :done, a3, 2023-08-06, 4d
    Develop in-house mapping style :done, a4, 2023-08-10, 5d

```
> Notes <br>
\- OSOS stands for "**O**pen**S**hift **O**bject**S**torage.  
\-  The `x` axis is there to indicate sequence rather than actual dates. 


Generated mostly through this interaction with ChatGPT : 
"Can you write mermaid.js code for a gantt diagram titled "PMTiles" with the following items that are to be done sequentially :
Step 1 : Upload a PMTile file to OSOS (e.g. OSM)
Step 2 : Test with a Leaflet client
Step 3 : K8S CronJob to keep file updated
Step 4 : Develop in-house mapping style"
