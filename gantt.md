```mermaid
gantt
    dateFormat  YYYY-MM-DD
    title       Adding GANTT diagram functionality to mermaid
    excludes    weekends
    %% (`excludes` accepts specific dates in YYYY-MM-DD format, days of the week ("sunday") or "weekends", but not the word "weekdays".)

    section Formation
    Completed task            :done,    des1, 2014-01-06,2014-01-08
    Crit completed task            :crit,done,    des19, 2014-01-06,2014-01-08
    Active task               :active,  des2, 2014-01-09, 3d
    Future task               :         des3, after des2, 5d
    Future task2              :         des4, after des3, 5d

    section Exploitation
    Describe gantt syntax               :active, a1, after des1, 3d
    Add gantt diagram to demo page      :after a1  , 20h
    Add another diagram to demo page    :doc1, after a1  , 48h

    section Daniel
    Describe gantt syntax               :after doc1, 3d
    Add gantt diagram to demo page      :20h
    Add another diagram to demo page    :48h
       
```

# Formation
2019-20	Formation Intro + Avancé (U de S) / Anglais – Français

2020-21	Formation sur les Plugins

2020-21	Formation QGIS Server

# Exploitation
2020-21	Audit du projet RHN

2020-21	Développement outils + Core


# Daniel
2019-20	Contrat de développement du Modeller

2021-21	Publication dans le repo de QGIS le plugin « Geo Simplification »

